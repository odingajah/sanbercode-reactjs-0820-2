import React, {Component} from 'react';
//import logo from './logo.svg';
import './App.css';
// import Tugas9 from './Tugas-9/Tugas9.js'
// import Tugas10 from './Tugas-10/Tugas10.js'
// import Timer from './Tugas-11/Tugas11.js'
// import Lists from './Tugas-12/TugasList.js'
// import DataHargaBuahAxios from './Tugas-13/Tugas13.js'
// import DaftarBuah from './Tugas-14/Main.js'

import { BrowserRouter as Router } from "react-router-dom";
import Nav from './Tugas-15/Nav'
import Routes from './Tugas-15/Routes'

class App extends Component {
  render() {
    return (
        <div className="App">
          <Router>
            <Nav />
            <Routes />
          </Router>
        </div>
    )
  }
}

// function App() {
//   return (
//   <div>
//     {
//     // <Lists/>
//     /* <Tugas9/>
//     <Tugas10/>
//     <Timer/> */
//     // <DataHargaBuahAxios/>
//     <DaftarBuah/>
//     }
//   </div>

//   );
// }

export default App