import React from "react";
import Tugas9 from '../Tugas-9/Tugas9.js'
import Tugas10 from '../Tugas-10/Tugas10.js'
import Timer from '../Tugas-11/Tugas11.js'
import Lists from '../Tugas-12/TugasList.js'
import DataHargaBuahAxios from '../Tugas-13/Tugas13.js'
import DaftarBuah from '../Tugas-14/Main.js'
import { Switch, Route } from "react-router";

const Routes = () => {

  return (
    <Switch>
      <Route exact path="/">
        <DaftarBuah />
      </Route>
      <Route path="/tugas13">
        <DataHargaBuahAxios />
      </Route>
      <Route exact path="/tugas12">
        <Lists />
      </Route>
      <Route exact path="/tugas11">
        <Timer />
      </Route>
      <Route path="/tugas10">
        <Tugas10 />
      </Route>
      <Route exact path="/tugas9">
        <Tugas9 />
      </Route>
    </Switch>
  );
};

export default Routes;
