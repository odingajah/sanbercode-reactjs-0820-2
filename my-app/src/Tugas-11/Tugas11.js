import React, {Component} from 'react'

var date = new Date();

export default class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      timer: 120,
      j: date.getHours(),
      m: date.getMinutes(),
      d: date.getSeconds(),
      formatWaktu:"AM",
      waktuDitampilkan: true,
      malam: false
    }
  }

  componentDidMount(){
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  componentDidUpdate(){
      if(this.state.waktuDitampilkan === true){
          if(this.state.timer <= 0){
              this.setState({ 
                  waktuDitampilkan: false
                })
              this.componentWillUnmount()
          }
      }
  }

  tick() {
    this.setState({
      timer: this.state.timer - 1,
      d: this.state.d + 1
    });
    if(this.state.j > 12){
        this.setState({j: this.state.j - 12})
        this.setState({malam: false})
    }
    if(this.state.m == 60){
        this.setState({m: this.state.j + 1})
        this.setState({m: 0})
    }
    if(this.state.d == 60){
        this.setState({d: this.state.m + 1})
        this.setState({d: 0})
    }
    if(this.state.j == 12 && this.state.malam == false){
        this.setState({j: 0})
        this.setState({malam: true})
    }
    if(this.state.j == 12 && this.state.malam == true){
        this.setState({j: 0})
        this.setState({malam: false})
    }
    if (this.state.malam == true){
        this.setState({formatWaktu:"PM"})
    }
    if (this.state.malam == false){
        this.setState({formatWaktu:"AM"})
    }
  }

  render(){
    return(
      <>
      {this.state.waktuDitampilkan && (
        <>
        <h1 style={{textAlign:"right", marginRight:"10%"}}>
            hitung mundur:{this.state.timer}
        </h1>
        <h1 style={{textAlign:"left", marginLeft:"10%", marginTop:"-65px"}}>
            sekarang jam:{this.state.j}:{this.state.m}:{this.state.d} {this.state.formatWaktu}
        </h1>
        </>
      )}
      </>
    )
  }
}