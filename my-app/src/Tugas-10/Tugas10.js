import React from 'react';
import '../App.css'

let dataHargaBuah = [
    {name: "Semangka", price: 10000, weight: 1000},
    {name: "Anggur", price: 40000, weight: 500},
    {name: "Strawberry", price: 30000, weight: 400},
    {name: "Jeruk", price: 30000, weight: 1000},
    {name: "Mangga", price: 30000, weight: 500}
]

class InfoHarga extends React.Component {
    render() {
        return (
            <>
                {dataHargaBuah.map(el=> {
                    return (
                        <tr>
                            <td>{el.name}</td>
                            <td>{el.price}</td>
                            <td>{el.weight/1000} Kg</td>
                        </tr>
                    )
                })}
        </>
        )
    }
}

export default class Tugas10 extends React.Component{    
    render(){
        return (
            <div>
                <h1 style = {{textAlign: "center"}}>Tabel Harga Buah</h1>
                <table>
                    <tr>
                        <th style={{width : "300px", backgroundColor: "#coral"}}>Nama</th>
                        <th style={{width : "200px", backgroundColor: "#coral"}}>Harga</th>
                        <th style={{width : "190px", backgroundColor: "#coral"}}>Berat</th>
                    </tr>
                    <InfoHarga/>
                </table>
            </div>
        )
    }
}